package com.kinn.extensions

import android.app.Activity
import android.content.Intent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.ImageView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair


fun Activity.startActivityWithSharedElement(
    intent: Intent,
    vararg sharedElements: Pair<View, String>
) {
    val activityOptionsCompat =
        ActivityOptionsCompat.makeSceneTransitionAnimation(this, *sharedElements)
    startActivity(intent, activityOptionsCompat.toBundle())
}


fun ImageView.rotate() {
    val mRotate = RotateAnimation(
        0F,
        180F,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    mRotate.duration = 200
    mRotate.interpolator = AccelerateDecelerateInterpolator()
    startAnimation(mRotate)
}

fun ImageView.rotateExpand() {
    val mRotate = RotateAnimation(
        0F,
        90F,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    mRotate.duration = 200
    mRotate.fillAfter = true
    mRotate.interpolator = AccelerateDecelerateInterpolator()
    startAnimation(mRotate)
}

fun ImageView.rotateCollapse() {
    val mRotate = RotateAnimation(
        90F,
        0F,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    mRotate.duration = 200
    mRotate.fillAfter = true
    mRotate.interpolator = AccelerateDecelerateInterpolator()
    startAnimation(mRotate)
}

fun ImageView.rotateUp() {
    val mRotate = RotateAnimation(
        0F,
        180F,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    mRotate.duration = 200
    mRotate.fillAfter = true
    mRotate.interpolator = AccelerateDecelerateInterpolator()
    startAnimation(mRotate)
}

fun ImageView.rotateDown() {
    val mRotate = RotateAnimation(
        180F,
        0F,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    mRotate.duration = 200
    mRotate.fillAfter = true
    mRotate.interpolator = AccelerateDecelerateInterpolator()
    startAnimation(mRotate)
}