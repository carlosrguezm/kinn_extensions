package com.kinn.extensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import java.io.File


fun Context.openPdf(path: String, applicationId: String) {
    val uri = FileProvider.getUriForFile(
        this,
        applicationId,
        File(path)
    )
    val pdfIntent = Intent(Intent.ACTION_VIEW)
    pdfIntent.setDataAndType(uri, "application/pdf")
    pdfIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    pdfIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    try {
        startActivity(pdfIntent)
    } catch (e: ActivityNotFoundException) {
        e.printStackTrace()
    }
}

fun Context.openInternalUrl(url: String, actionbarPrimaryColor: Int, actionbarSecondaryColor: Int) {
    val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
    val customTabsIntent: CustomTabsIntent = builder.build()
    customTabsIntent.intent.putExtra(
        CustomTabsIntent.EXTRA_TOOLBAR_COLOR,
        ContextCompat.getColor(this, actionbarPrimaryColor)
    )
    customTabsIntent.intent.putExtra(
        CustomTabsIntent.EXTRA_SECONDARY_TOOLBAR_COLOR,
        ContextCompat.getColor(this, actionbarSecondaryColor)
    )
    customTabsIntent.launchUrl(this, Uri.parse(url))
}

fun Context.openUrl(url: String) {
    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    try {
        startActivity(browserIntent)
    } catch (e: ActivityNotFoundException) {
        startActivity(Intent.createChooser(browserIntent, null))
    }
}

fun Activity.shareUrl(
    text: String,
    subject: String? = null,
    titleShareWith: String
) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        if (!TextUtils.isEmpty(subject)) {
            intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        }

        val chooserIntent = Intent.createChooser(
            intent,
            titleShareWith
        )
        startActivity(chooserIntent)

    } catch (ignored: Exception) {
    }
}
