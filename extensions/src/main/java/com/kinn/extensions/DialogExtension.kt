package com.kinn.extensions

import android.app.Activity
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 *
 * @receiver Activity
 * @param positiveButtonText String
 * @param message String
 * @param onClickListener Function2<Any, Any, Unit>?
 * @param cancelable Boolean
 */
fun Activity.showDialog(
    positiveButtonText: String,
    message: String,
    onClickListener: ((Any, Any) -> Unit)?,
    cancelable: Boolean) {

    val builder = MaterialAlertDialogBuilder(this)
    builder.setCancelable(cancelable)
    builder.setMessage(message)
    builder.setPositiveButton(positiveButtonText, onClickListener)
    builder.create().show()
}